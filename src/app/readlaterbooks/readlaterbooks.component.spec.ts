import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadlaterbooksComponent } from './readlaterbooks.component';

describe('ReadlaterbooksComponent', () => {
  let component: ReadlaterbooksComponent;
  let fixture: ComponentFixture<ReadlaterbooksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReadlaterbooksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadlaterbooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
