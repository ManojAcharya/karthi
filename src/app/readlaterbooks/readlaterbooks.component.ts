import { Component, OnInit } from '@angular/core';
import { BookserviceService } from '../service/bookservice.service';
import { Books } from '../model/Book';

@Component({
  selector: 'app-readlaterbooks',
  templateUrl: './readlaterbooks.component.html',
  styleUrls: ['./readlaterbooks.component.css']
})
export class ReadlaterbooksComponent implements OnInit {

  books: Books[];

  favlist: Books;

  constructor(private bs: BookserviceService) {
    this.favlist = new Books('', '', '');
    this.bs.favlist.forEach(a => {
      this.favlist.id = a.id;
      this.favlist.author = a.author;
      this.favlist.title = a.title;
      this.favlist.year = a.year;
    });
    console.log('///////******');
    console.log(this.favlist);
  }

  ngOnInit(): void {
    this.bs.favlist.forEach(a => {
      this.favlist.id = a.id;
      this.favlist.author = a.author;
      this.favlist.title = a.title;
      this.favlist.year = a.year;
    });
    console.log('******');
    console.log(this.favlist);
  }
}
