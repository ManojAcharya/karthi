export class Books{
    constructor(public title:string,
        public author:string,
        public year:string,
        public id?:number){}
}