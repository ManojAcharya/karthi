import { Component, OnInit } from '@angular/core';
import { Books } from '../model/Book';
import { BookserviceService } from '../service/bookservice.service';
import { books } from '../model/Bookslist';


@Component({
  selector: 'app-likedbooks',
  templateUrl: './likedbooks.component.html',
  styleUrls: ['./likedbooks.component.css']
})
export class LikedbooksComponent implements OnInit {

  books: Books[];

  favlist: Books;

  constructor(private bs: BookserviceService) {
    this.favlist = new Books('','','');
    this.bs.favlist.forEach(a => { 
      this.favlist.id=a.id;
      this.favlist.author=a.author;
      this.favlist.title=a.title;
      this.favlist.year=a.year; 
    });
    console.log('///////******');
    console.log(this.favlist);
  }

  

  ngOnInit(): void {
    this.bs.favlist.forEach(a => { 
      this.favlist.id=a.id;
      this.favlist.author=a.author;
      this.favlist.title=a.title;
      this.favlist.year=a.year; 
    });
    console.log('******');
    console.log(this.favlist);
  }

}
