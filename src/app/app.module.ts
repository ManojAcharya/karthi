import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BooksdisplayComponent } from './booksdisplay/booksdisplay.component';
import { BookblogComponent } from './bookblog/bookblog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BlogeditComponent } from './bookedit/blogedit.component';
import { BlogformComponent } from './booksform/blogform.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LikedbooksComponent } from './likedbooks/likedbooks.component';
import { ReadlaterbooksComponent } from './readlaterbooks/readlaterbooks.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    BooksdisplayComponent,
    BookblogComponent,
    BlogeditComponent,
    BlogformComponent,
    LoginComponent,
    RegisterComponent,
    LikedbooksComponent,
    ReadlaterbooksComponent,
    PagenotfoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
