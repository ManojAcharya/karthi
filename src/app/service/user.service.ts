import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../model/user';
import { users } from '../model/userList';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private router: Router) { }

  users:Array<LoginUser> = users;

  isLoggedIn() {
    return !!localStorage.getItem('email');
  }

  checkLogin(user:LoginUser):boolean{
    for(let usr of this.users){
      if(user.email===usr.email && user.password===usr.password)
        return true;
    }
    return false;
  }

  logout(){
    localStorage.removeItem('email');
    this.router.navigate(['bookslist']);
  }

}
